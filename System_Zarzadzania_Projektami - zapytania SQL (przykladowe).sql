--1. Liczba pracownikow wedlug miejsca zamieszkania, z rozroznieniem typu pracownikow na pracownikow wewn. i zewn.
--, posorotwane rosnaco po typie pracownika, malejaco po ilosci pracownikow i rosnaco po miejscowosci

SELECT 'Prac. Wewn.' TYP_PRAC
        , M.NAZWA NAZWA_MIEJSC
        , COUNT(*) LICZBA_PRACOWNIKOW
FROM 
        PRACWEWN PW
        JOIN PRACOWNIK P ON PW.IDPRACOWNIKA = P.IDPRACOWNIKA
        JOIN MIASTO M ON P.IDMIASTA = M.IDMIASTA
GROUP BY 
      M.NAZWA

UNION

SELECT 'Prac. Zewn.'
        , M.NAZWA, COUNT(*)
FROM 
        PRACZEWN PZ
        JOIN PRACOWNIK P ON PZ.IDPRACOWNIKA = P.IDPRACOWNIKA
        JOIN MIASTO M ON P.IDMIASTA = M.IDMIASTA
GROUP BY 
         M.NAZWA
ORDER BY 
        TYP_PRAC
        , LICZBA_PRACOWNIKOW DESC
        , NAZWA_MIEJSC;
        
        



--2. Jakie zadania wciaz nie zostaly zakonczone, wraz z podaniem nazwy projektu, nazwy fazy oraz imenia i nazwiska przypisanego pracownika, a takze dlugoscia trwania zadania (w dniach). 
--Podac tylko te zadania, ktorych czas trwania przekracza 100 dni. Posortowane po dacie rozpoczecia projektu i zadania

SELECT P.NAZWA NAZWA_PROJ
        , F.NAZWA NAZWA_FAZY 
        , R.OPISZADANIA, R.DATAROZP DATAROZP_ZAD, R.DATAZAK DATAZAK_ZAD, R.CZYSUKCES CZYSUKCES_ZAD
        , PR.IMIE, PR.NAZWISKO
        , (TRUNC(SYSDATE) - R.DATAROZP) ILOSC_DNI
FROM REALIZACJAPROJ R
      JOIN PROJEKT P ON R.IDPROJEKTU = P.IDPROJEKTU
      JOIN FAZAPROJ F ON R.IDFAZY = F.IDFAZY
      JOIN PRACOWNIK PR ON PR.IDPRACOWNIKA = R.IDPRACOWNIKA
WHERE 
      R.CZYSUKCES IS NULL
      AND (TRUNC(SYSDATE) - R.DATAROZP) > 100
ORDER BY
      P.DATAROZP
      , R.DATAROZP;





--3. Lista projektow, wraz z liczba ich faz, zadan oraz srednia iloscia zadan w fazie. Kazdy projekt powinien miec dodatkowo opis: "sukces", "trwa", "porazka" (bazujac na polu CZYSUKCES z tabeli PROJEKT). 
--Posortowane rosnaco wedlug daty rozpoczecia projektu

SELECT P.IDPROJEKTU, P.NAZWA NAZWA_PROJ, P.DATAROZP, P.DATAZAK
      , CASE
          WHEN P.CZYSUKCES = 1 THEN 'sukces'
          WHEN P.CZYSUKCES = 0 THEN 'porazka'
          WHEN P.CZYSUKCES IS NULL THEN 'trwa'
          ELSE 'n/a'
      END STATUS_PROJ
      , COUNT(*) ILOSC_FAZ
      , Z.ILOSC_ZADAN 
      , ROUND(Z.ILOSC_ZADAN/COUNT(*), 2) SR_ILOSC_ZAD_W_FAZIE
FROM PROJEKT P
      JOIN STATUSFAZYPROJ S ON P.IDPROJEKTU = S.IDPROJEKTU
      JOIN (
            SELECT R.IDPROJEKTU, COUNT(*) ILOSC_ZADAN
            FROM REALIZACJAPROJ R
            GROUP BY
                  R.IDPROJEKTU
            ) Z ON P.IDPROJEKTU = Z.IDPROJEKTU
GROUP BY
      P.IDPROJEKTU
      , P.NAZWA
      , P.DATAROZP
      , P.DATAZAK
      , CASE
          WHEN P.CZYSUKCES = 1 THEN 'sukces'
          WHEN P.CZYSUKCES = 0 THEN 'porazka'
          WHEN P.CZYSUKCES IS NULL THEN 'trwa'
          ELSE 'n/a'
        END
      , Z.ILOSC_ZADAN
ORDER BY
      P.DATAROZP;




--4. Lista pracownikow, ktorzy pracowali nad wiecej niz 2 zadaniami lub pracowali nad 1 zadaniem srednio ponad 500 dni, z liczba projektow i zadan, w ktorych uczestniczyli 
-- oraz srednim czasem spedzonym nad pojedyczym zadaniem (w dniach), posortowane malejaco po srednim czasie
-- Uwaga: w tabeli REALIZACJAPROJ jesli zadanie nie zostalo jeszcze zakonczone, pole DATAZAK ma wartosc NULL, w takim przypadku przyjeto date dzisiejsza

SELECT P.IDPRACOWNIKA
      , P.IMIE
      , P.NAZWISKO
      , P.DATAURODZENIA
      , COUNT(RP.IDPROJEKTU) LICZBA_PROJ
      , SUM(RP.LICZBA_ZADAN) LICZBA_ZADAN
      , ROUND(SUM(RP.ILOSC_DNI)/SUM(RP.LICZBA_ZADAN),0) SR_ILOSC_DNI
FROM 
      PRACOWNIK P 
      JOIN (
            SELECT IDPRACOWNIKA
                    , IDPROJEKTU
                    , COUNT(*) LICZBA_ZADAN
                    , SUM(NVL(DATAZAK, TRUNC(SYSDATE)) - DATAROZP) ILOSC_DNI
            FROM 
                    REALIZACJAPROJ
            GROUP BY 
                    IDPRACOWNIKA
                    , IDPROJEKTU
           ) RP ON P.IDPRACOWNIKA = RP.IDPRACOWNIKA
GROUP BY
        P.IDPRACOWNIKA
        , P.IMIE
        , P.NAZWISKO
        , P.DATAURODZENIA
HAVING
      SUM(RP.LICZBA_ZADAN) > 2 
      OR ROUND(SUM(RP.ILOSC_DNI)/SUM(RP.LICZBA_ZADAN),0) > 500
ORDER BY
         SR_ILOSC_DNI DESC;
         



--5. Do zrealizowania jest wazny projekt, nalezy znalezc pracownikow ktorzy:
--  - pracowali przy wiecej niz 1 projekcie
--  - znaja Jave lub C++ oraz SQL (Oracle lub MSSQL)
--  - mieszkaja w Warszawie
--  - sa pracownikami wewnetrznymi

SELECT P.IDPRACOWNIKA, P.IMIE, P.NAZWISKO
FROM PRACOWNIK P
      JOIN PRACWEWN PW ON P.IDPRACOWNIKA = PW.IDPRACOWNIKA
      JOIN MIASTO M ON M.IDMIASTA = P.IDMIASTA
      JOIN (
            SELECT DISTINCT IDPROJEKTU, IDPRACOWNIKA
            FROM REALIZACJAPROJ
            ) R ON R.IDPRACOWNIKA = P.IDPRACOWNIKA
WHERE
      M.NAZWA = 'Warszawa'
      AND EXISTS(
                SELECT *
                FROM UMIEJETNOSCPRAC U
                WHERE U.IDPRACOWNIKA = P.IDPRACOWNIKA
                      AND (U.NAZWASKR = 'Java' OR U.NAZWASKR = 'C++')
                      
                )
      AND EXISTS(
                SELECT *
                FROM UMIEJETNOSCPRAC U
                WHERE U.IDPRACOWNIKA = P.IDPRACOWNIKA
                      AND (U.NAZWASKR = 'MSSQL' OR U.NAZWASKR = 'OSQL')
                )
GROUP BY
      P.IDPRACOWNIKA
      , P.IMIE
      , P.NAZWISKO
HAVING
      COUNT(R.IDPROJEKTU) > 1;
      



--6. Wypisac kierownikow projektow (id pracownika, imie, nazwisko), 
--podac dane o projekcie: nazwa projektu, data rozpoczecia, data zakonczenia oraz status projektu (jako dodatkowa kolumna StatusProjektu z wartosciami "sukces", "porazka", "trwa" na podstawie kolumny CZYSUKCES)
--sprawdzic czy kierownik bral udzial przy wykonywaniu zadan projektu (dodac dodatkowa kolumne CzyBralUdzial z wartosciami "tak" lub "nie"),
--sprawdzic czy zna metodyke Prince2 (kolumna Prince2 z wartosciami "tak" lub "nie"),
--podac liczbe pracownikow ktorymi zarzadzal kierownik

SELECT  PR.IDPRACOWNIKA, PR.IMIE, PR.NAZWISKO
        , P.NAZWA, P.DATAROZP, P.DATAZAK 
        , CASE
            WHEN P.CZYSUKCES = 1 THEN 'sukces'
            WHEN P.CZYSUKCES = 0 THEN 'trwa'
            WHEN P.CZYSUKCES IS NULL THEN 'porazka'
            ELSE 'n/a'
        END StatusProjektu
        , NVL(R.czyBralUdzial, 'nie') CzyBralUdzial
        , NVL(U.czyPrince2, 'nie') Prince2
        , COUNT(R2.IDPRACOWNIKA) LiczbaPracownikow

FROM PROJEKT P
      JOIN PRACOWNIK PR ON P.KIEROWNIKPROJ = PR.IDPRACOWNIKA
      LEFT JOIN (
                SELECT DISTINCT IDPROJEKTU, IDPRACOWNIKA, 'tak' czyBralUdzial
                FROM REALIZACJAPROJ
                ) R ON P.KIEROWNIKPROJ = R.IDPRACOWNIKA AND P.IDPROJEKTU = R.IDPROJEKTU
      LEFT JOIN (
                SELECT DISTINCT IDPROJEKTU, IDPRACOWNIKA
                FROM REALIZACJAPROJ
                ) R2 ON P.IDPROJEKTU = R2.IDPROJEKTU
      LEFT JOIN (
                SELECT IDPRACOWNIKA, 'tak' czyPrince2
                FROM UMIEJETNOSCPRAC 
                WHERE NAZWASKR = 'Prince2'
                ) U ON P.KIEROWNIKPROJ = U.IDPRACOWNIKA
                                      
GROUP BY
        PR.IDPRACOWNIKA, PR.IMIE, PR.NAZWISKO
        , P.NAZWA, P.DATAROZP, P.DATAZAK 
        , CASE
            WHEN P.CZYSUKCES = 1 THEN 'sukces'
            WHEN P.CZYSUKCES = 0 THEN 'trwa'
            WHEN P.CZYSUKCES IS NULL THEN 'porazka'
            ELSE 'n/a'
        END
        , NVL(R.czyBralUdzial, 'nie')
        , NVL(U.czyPrince2, 'nie')
ORDER BY
      P.DATAROZP;
      




--7.Stworzyc tabele z lista pracownikow pracujacych przy danym projekcie. Poda� nazwe projektu, nr porzadkowy pracownika na liscie (dla kazdego projektu powinien zaczynac sie od 1), imie, nazwisko 
-- oraz dodatkowa kolumne Kierownik przyjmujaca 3 wartosci: "kierownik", "zastepca" lub "" (pusta wartosc jesli pracownik nie jest kierownikiem lub zastepca)
-- Posortowac dane po nazwie projektu, kolumnie Kierownik (pierwszy powinien byc kierownik, potem zastepca, a potem pozostali pracownicy), a potem rosnaco po nazwisku.

SELECT P.NAZWA NAZWAPROJ
        , DENSE_RANK() OVER (PARTITION BY P.IDPROJEKTU 
                              ORDER BY 
                                      CASE R.IDPRACOWNIKA
                                          WHEN P.KIEROWNIKPROJ THEN 1
                                          WHEN P.ZASTKIEROWNIKA THEN 2
                                          ELSE 3
                                      END
                                      , PR.NAZWISKO
                            ) "LP."
        , PR.IMIE, PR.NAZWISKO
        , CASE R.IDPRACOWNIKA
              WHEN P.KIEROWNIKPROJ THEN 'kierownik'
              WHEN P.ZASTKIEROWNIKA THEN 'zastepca'
              ELSE ' '
        END KIEROWNIK
FROM (
        SELECT IDPROJEKTU, IDPRACOWNIKA
        FROM REALIZACJAPROJ
        
        UNION
        
        SELECT IDPROJEKTU, KIEROWNIKPROJ --kierownik moze nie miec przypisanego zadania w tabeli REALIZACJAPROJ
        FROM PROJEKT
        
        UNION
        
        SELECT IDPROJEKTU, ZASTKIEROWNIKA --zastepca moze nie miec przypisanego zadania w tabeli REALIZACJAPROJ
        FROM PROJEKT
      ) R
      JOIN PROJEKT P ON P.IDPROJEKTU = R.IDPROJEKTU
      JOIN PRACOWNIK PR ON PR.IDPRACOWNIKA = R.IDPRACOWNIKA

ORDER BY
      P.NAZWA
      , CASE KIEROWNIK
          WHEN 'kierownik' THEN 1
          WHEN 'zastepca' THEN 2
          ELSE 3
      END
      , PR.NAZWISKO;





--8. Zrobic zestawienie wszystkich umiejetnosci w bazie (z podaniem ich pelnej nazwy) z przypisaniem ilu pracownikow je posiada, przy czym nalezy pokazac w oddzielnych kolumnach pracownikow wewn. i zewn.
--(1.sposob - podzapytania w SELECT)
SELECT U.NAZWAPELNA
     
      , (
        SELECT COUNT(*)
        FROM UMIEJETNOSCPRAC UP
              JOIN PRACWEWN P ON UP.IDPRACOWNIKA = P.IDPRACOWNIKA AND UP.NAZWASKR = U.NAZWASKR
        WHERE
            UP.NAZWASKR = U.NAZWASKR
        ) PRAC_WEWN
      
      , (
        SELECT COUNT(*)
        FROM UMIEJETNOSCPRAC UP2
              JOIN PRACZEWN P ON UP2.IDPRACOWNIKA = P.IDPRACOWNIKA AND UP2.NAZWASKR = U.NAZWASKR
        WHERE
            UP2.NAZWASKR = U.NAZWASKR
        ) PRAC_ZEWN       
                
FROM 
    UMIEJETNOSC U
ORDER BY
    U.NAZWAPELNA;


--(2.sposob - bez podzapytan w SELECT z u�yciem UNION)
SELECT X.NAZWAPELNA, COUNT(PRAC_WEWN) PRAC_WEWN, COUNT(PRAC_ZEWN) PRAC_ZEWN
FROM(
      SELECT U.NAZWAPELNA, P.IDPRACOWNIKA PRAC_WEWN, NULL PRAC_ZEWN
      FROM UMIEJETNOSC U
            JOIN UMIEJETNOSCPRAC UP ON U.NAZWASKR = UP.NAZWASKR
            JOIN PRACWEWN P ON UP.IDPRACOWNIKA = P.IDPRACOWNIKA
      
      UNION
      
      SELECT U.NAZWAPELNA, NULL, P.IDPRACOWNIKA
      FROM UMIEJETNOSC U
            JOIN UMIEJETNOSCPRAC UP ON U.NAZWASKR = UP.NAZWASKR
            JOIN PRACZEWN P ON UP.IDPRACOWNIKA = P.IDPRACOWNIKA
      ) X
GROUP BY
      X.NAZWAPELNA
ORDER BY
    X.NAZWAPELNA;


--(3.sposob - bez podzapytan i UNION)
SELECT U.NAZWAPELNA, COUNT(PW.IDPRACOWNIKA) PRAC_WEWN, COUNT(PZ.IDPRACOWNIKA) PRAC_ZEWN
FROM UMIEJETNOSC U
      JOIN UMIEJETNOSCPRAC UP ON U.NAZWASKR = UP.NAZWASKR
      LEFT JOIN PRACWEWN PW ON UP.IDPRACOWNIKA = PW.IDPRACOWNIKA
      LEFT JOIN PRACZEWN PZ ON UP.IDPRACOWNIKA = PZ.IDPRACOWNIKA
GROUP BY
    U.NAZWAPELNA
ORDER BY
    U.NAZWAPELNA;






--9. Znalezc pracownikow ktorzy brali udzial tylko w projektach i fazach zakonczonych sukcesem oraz ktorzy wykonali powierzone im zadania pomyslnie. Sprawdzic ktory z tych pracownikow wykonal najwiecej zadan
WITH NIEZAWODNI_PRAC (IDPRACOWNIKA, IMIE, NAZWISKO, ILOSC_ZADAN)
AS
(
  SELECT R.IDPRACOWNIKA, PR.IMIE, PR.NAZWISKO, COUNT(*) ILOSC_ZADAN
  FROM REALIZACJAPROJ R
      JOIN STATUSFAZYPROJ S ON R.IDPROJEKTU = S.IDPROJEKTU AND R.IDFAZY = S.IDFAZY
      JOIN PROJEKT P ON P.IDPROJEKTU = S.IDPROJEKTU
      JOIN PRACOWNIK PR ON PR.IDPRACOWNIKA = R.IDPRACOWNIKA
  WHERE
      R.CZYSUKCES = 1
      AND S.CZYSUKCES = 1
      AND P.CZYSUKCES = 1
      AND NOT EXISTS (
                      SELECT R2.IDPRACOWNIKA
                      FROM REALIZACJAPROJ R2
                            JOIN STATUSFAZYPROJ S2 ON R2.IDPROJEKTU = S2.IDPROJEKTU AND R2.IDFAZY = S2.IDFAZY
                            JOIN PROJEKT P2 ON P2.IDPROJEKTU = S2.IDPROJEKTU
                      WHERE
                            R.IDPRACOWNIKA = R2.IDPRACOWNIKA
                            AND (
                                R2.CZYSUKCES <> 1
                                OR S2.CZYSUKCES <> 1
                                OR P2.CZYSUKCES <> 1
                                OR R2.CZYSUKCES IS NULL
                                OR S2.CZYSUKCES IS NULL
                                OR P2.CZYSUKCES IS NULL
                                )
                      )

  GROUP BY
      R.IDPRACOWNIKA
      , PR.IMIE
      , PR.NAZWISKO
)



SELECT NP.IDPRACOWNIKA, NP.IMIE, NP.NAZWISKO, ILOSC_ZADAN
FROM NIEZAWODNI_PRAC NP
WHERE
    NP.ILOSC_ZADAN = (
                      SELECT MAX(NP2.ILOSC_ZADAN)
                      FROM NIEZAWODNI_PRAC NP2
                      );





--10. Podac nazwy projektow, fazy o najwiekszym udziale trwania w projekcie, czas trwania faz (w miesiacach) oraz udzial. Pokazac tylko fazy zakonczone sukcesem, nie uwzgledniac projektow w trakcie. 
-- Posortowac po dacie rozpoczecia projektu.
SELECT P.NAZWA NAZWA_PROJ
      , F.NAZWA NAZWA_FAZY
      , ROUND(MONTHS_BETWEEN(S.DATAZAK, S.DATAROZP),0) DL_TRWANIA
      , 100*ROUND(
                  MONTHS_BETWEEN(S.DATAZAK, S.DATAROZP)
                    / MONTHS_BETWEEN(P.DATAZAK, P.DATAROZP) 
                  , 2
                  )  || '%'  UDZIAL_W_PROJEKCIE
FROM PROJEKT P
      JOIN STATUSFAZYPROJ S ON P.IDPROJEKTU = S.IDPROJEKTU
      JOIN FAZAPROJ F ON F.IDFAZY = S.IDFAZY
WHERE
    S.CZYSUKCES = 1
    AND P.CZYSUKCES IS NOT NULL
    AND (S.DATAZAK - S.DATAROZP) >= ALL ( 
                                          SELECT (S2.DATAZAK - S2.DATAROZP)
                                          FROM STATUSFAZYPROJ S2
                                          WHERE
                                              S2.IDPROJEKTU = S.IDPROJEKTU
                                              AND S2.CZYSUKCES = 1
                                          )
ORDER BY
    P.DATAROZP;
                                                          

  



















