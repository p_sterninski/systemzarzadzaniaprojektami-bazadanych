
--tabela MIASTO
INSERT ALL
INTO MIASTO(IdMiasta, Nazwa) 
  VALUES(1, 'Warszawa')
INTO MIASTO(IdMiasta, Nazwa) 
  VALUES(2, 'Lomianki')
INTO MIASTO(IdMiasta, Nazwa) 
  VALUES(3, 'Marki')
INTO MIASTO(IdMiasta, Nazwa) 
  VALUES(4, 'Pruszkow')
INTO MIASTO(IdMiasta, Nazwa) 
  VALUES(5, 'Piaseczno')
INTO MIASTO(IdMiasta, Nazwa) 
  VALUES(6, 'Otwock')
SELECT * FROM DUAL;


--tabela PRACOWNIK
INSERT ALL
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(1, 'Piotr' , 'Nowak' , '1990-03-07' , 'Polna 1/11 01-001' , 1)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(2, 'Andrzej' , 'Kowalski' , '1967-01-02' , 'Lesna 2 02-002' , 5)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(3, 'Jan' , 'Wisniewski' , '1968-05-14' , 'Sloneczna 3/13 03-133' , 1)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(4, 'Stanislaw' , 'Kowalczyk' , '1981-08-04' , 'Krotka 4/44 04-444' , 4)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(5, 'Tomasz' , 'Kaminski' , '1979-06-12' , 'Szkolna 5 05-511' , 1)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(6, 'Pawel' , 'Zielinski' , '1977-03-05' , 'Ogrodowa 6/76 06-666' , 1)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(7, 'Marcin' , 'Szymanski' , '1975-01-29' , 'Brzozowa 7/17 07-007' , 2)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(8, 'Michal' , 'Wozniak' , '1971-05-01' , 'Kwiatowa 8 08-808' , 1)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(9, 'Marek' , 'Kozlowski' , '1966-11-17' , 'Kosciuszki 9 09-999' , 5)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(10, 'Grzegorz' , 'Mazur' , '1970-09-06' , 'Mickiewicza 10/1 10-100' , 2)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(11, 'Anna' , 'Wojciechowska' , '1973-03-16' , 'Sienkiewicza 11/2 11-111' , 1)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(12, 'Katarzyna' , 'Kwiastkowska' , '1977-12-19' , 'Konopnickiej 12 12-112' , 1)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(13, 'Malgorzata' , 'Krawczyk' , '1980-01-20' , 'Kopernika 13 13-130' , 1)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(14, 'Joanna' , 'Grabowska' , '1984-03-13' , 'Dolna 14 14-140' , 1)
INTO PRACOWNIK(IdPracownika, Imie, Nazwisko, DataUrodzenia, Adres, IdMiasta) 
  VALUES(15, 'Agnieszka' , 'Kaczmarek' , '1985-01-01' , 'Jasna 15/15 15-150' , 4)
SELECT * FROM DUAL;


--tabela PRACWEWN
INSERT ALL
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(1, 'Java Developer')
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(2, 'Graphic Designer')
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(3, 'DB Developer')
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(5, 'BI Developer')
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(6, 'Java Developer')
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(7, 'Javascript Developer')
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(8, 'Graphic Designer')
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(10, 'Analyst')
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(11, 'Analyst')
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(12, 'WebDesigner')
INTO PRACWEWN(IdPracownika, Stanowisko) 
  VALUES(15, 'Java Developer')
SELECT * FROM DUAL;


--tabela PRACZEWN
INSERT ALL
INTO PRACZEWN(IdPracownika, Email, Telefon, InnyKontakt) 
  VALUES(4, 's.kowalczyk@inne.com' , '123456789' , NULL)
INTO PRACZEWN(IdPracownika, Email, Telefon, InnyKontakt) 
  VALUES(9, NULL , '222333444' , NULL)
INTO PRACZEWN(IdPracownika, Email, Telefon, InnyKontakt) 
  VALUES(13, 'j.grabowska@cos.com' , NULL , 'skype: grab')
INTO PRACZEWN(IdPracownika, Email, Telefon, InnyKontakt) 
  VALUES(14, 'a.kaczmarek@firma.pl' , NULL , 'skype: kacz')
SELECT * FROM DUAL;


--tabela UMIEJETNOSC
INSERT ALL
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('C++' , 'C++')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('C' , 'C')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('Java' , 'Java')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('JavaSc' , 'JavaScript')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('MSSQL' , 'MS SQL Server')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('OSQL' , 'Oracle Database')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('Excel' , 'MS Office Excel')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('Word' , 'MS Office Word')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('Prince2' , 'Prince2')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('HTML' , 'HTML')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('PHP' , 'PHP')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('CSS' , 'CSS')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('PSP' , 'PaintShopPro')
INTO UMIEJETNOSC(NazwaSkr, NazwaPelna) 
  VALUES('CrlDrw' , 'CorelDraw')
SELECT * FROM DUAL;


--tabela UMIEJETNOSCPRAC
INSERT ALL
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(1, 'Java')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(1, 'C++')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(2, 'HTML')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(2, 'CrlDrw')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(2, 'PSP')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(3, 'MSSQL')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(3, 'OSQL')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(3, 'Excel')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(4, 'Prince2')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(4, 'Java')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(4, 'HTML')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(5, 'C++')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(5, 'Java')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(5, 'OSQL')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(5, 'Excel')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(5, 'Prince2')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(6, 'C')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(6, 'C++')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(6, 'Java')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(6, 'MSSQL')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(7, 'JavaSc')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr)
  VALUES(7, 'Java')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(7, 'PHP')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr)
  VALUES(8, 'PHP')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(8, 'PSP')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(8, 'CrlDrw')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(9, 'OSQL')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(9, 'Excel')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(9, 'C')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(10, 'C++')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(10, 'Excel')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(11, 'Excel')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(11, 'Word')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(12, 'HTML')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(12, 'PHP')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(12, 'CSS')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr)
  VALUES(13, 'C++')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(13, 'Java')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(13, 'OSQL')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(14, 'JavaSc')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(14, 'OSQL')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(14, 'Prince2')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr) 
  VALUES(15, 'Java')
INTO UMIEJETNOSCPRAC(IdPracownika, NazwaSkr)
  VALUES(15, 'MSSQL')
SELECT * FROM DUAL;


--tabela PROJEKT
INSERT ALL
INTO PROJEKT(IdProjektu, Nazwa, DataRozp, DataZak, CzySukces, KierownikProj, ZastKierownika) 
  VALUES(1, 'Projekt1' , '2005-11-13' , '2010-01-28' , 1, 4, 5)
INTO PROJEKT(IdProjektu, Nazwa, DataRozp, DataZak, CzySukces, KierownikProj, ZastKierownika) 
  VALUES(2, 'Projekt2' , '2009-04-22' , '2014-08-07' , 1, 6, NULL)
INTO PROJEKT(IdProjektu, Nazwa, DataRozp, DataZak, CzySukces, KierownikProj, ZastKierownika) 
  VALUES(3, 'Projekt3' , '2015-07-01' , NULL , NULL, 7, NULL)
INTO PROJEKT(IdProjektu, Nazwa, DataRozp, DataZak, CzySukces, KierownikProj, ZastKierownika) 
  VALUES(4, 'Projekt4' , '2016-02-13' , '2016-12-12' , 0, 5, 11)
INTO PROJEKT(IdProjektu, Nazwa, DataRozp, DataZak, CzySukces, KierownikProj, ZastKierownika) 
  VALUES(5, 'Projekt5' , '2016-05-29' , NULL , NULL, 14, 13)
SELECT * FROM DUAL;



--tabela FAZAPROJ
INSERT ALL
INTO FAZAPROJ(IdFazy, Nazwa) 
  VALUES(1, 'Faza przygotowawcza')
INTO FAZAPROJ(IdFazy, Nazwa) 
  VALUES(2, 'Faza wykonawcza')
INTO FAZAPROJ(IdFazy, Nazwa) 
  VALUES(3, 'Faza zamkniecia')
SELECT * FROM DUAL;


--tabela STATUSFAZYPROJ
INSERT ALL
INTO STATUSFAZYPROJ(IdProjektu, IdFazy, DataRozp, DataZak, CzySukces) 
  VALUES(1, 1, '2005-11-13' , '2007-01-10' , 1)
INTO STATUSFAZYPROJ(IdProjektu, IdFazy, DataRozp, DataZak, CzySukces) 
  VALUES(1, 2, '2007-01-11' , '2009-04-04' , 1)
INTO STATUSFAZYPROJ(IdProjektu, IdFazy, DataRozp, DataZak, CzySukces) 
  VALUES(1, 3, '2009-04-05' , '2010-01-28' , 1)
INTO STATUSFAZYPROJ(IdProjektu, IdFazy, DataRozp, DataZak, CzySukces) 
  VALUES(2, 1, '2009-04-22' , '2010-03-30' , 1)
INTO STATUSFAZYPROJ(IdProjektu, IdFazy, DataRozp, DataZak, CzySukces) 
  VALUES(2, 2, '2010-03-31' , '2013-05-11' , 1)
INTO STATUSFAZYPROJ(IdProjektu, IdFazy, DataRozp, DataZak, CzySukces) 
  VALUES(2, 3, '2013-05-12' , '2014-08-07' , 0)
INTO STATUSFAZYPROJ(IdProjektu, IdFazy, DataRozp, DataZak, CzySukces)
  VALUES(3, 1, '2015-07-01' , '2016-09-10' , 1)
INTO STATUSFAZYPROJ(IdProjektu, IdFazy, DataRozp, DataZak, CzySukces) 
  VALUES(3, 2, '2016-09-11' , NULL , NULL)
INTO STATUSFAZYPROJ(IdProjektu, IdFazy, DataRozp, DataZak, CzySukces) 
  VALUES(4, 1, '2016-02-13' , '2016-12-12' , 0)
INTO STATUSFAZYPROJ(IdProjektu, IdFazy, DataRozp, DataZak, CzySukces)
  VALUES(5, 1, '2016-05-29' , NULL , NULL)
SELECT * FROM DUAL;


--tabela REALIZCJAPROJ
INSERT ALL
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(1, 1, 4, 'Opis', '2005-11-15', '2006-12-20', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(1, 1, 5, 'Opis', '2005-11-20', '2007-01-05', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(1, 2, 3, 'Opis', '2007-01-11', '2009-01-10', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(1, 2, 6, 'Opis', '2007-02-20', '2009-02-01', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(1, 2, 12, 'Opis', '2007-03-03', '2009-03-15', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(1, 3, 4, 'Opis', '2009-04-05', '2010-01-28', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(2, 1, 6, 'Opis', '2009-04-30', '2010-03-20', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(2, 2, 9, 'Opis', '2010-04-01', '2013-02-11', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(2, 2, 7, 'Opis', '2010-04-20', '2013-02-28', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces)
  VALUES(2, 2, 8, 'Opis', '2010-05-02', '2013-05-03', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(2, 2, 15, 'Opis', '2010-05-17', '2013-04-15', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(2, 3, 7, 'Opis', '2013-06-21', '2014-06-30', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(2, 3, 4, 'Opis', '2013-05-15', '2014-08-07', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(3, 1, 7, 'Opis', '2015-07-05', '2016-08-15', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(3, 1, 14, 'Opis', '2015-07-10', '2016-09-03', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(3, 2, 15, 'Opis', '2016-09-14', NULL, NULL)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(3, 2, 3, 'Opis', '2016-09-23', NULL, NULL)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(4, 1, 10, 'Opis', '2016-02-22', '2016-12-01', 0)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(4, 1, 13, 'Opis', '2016-02-23', '2016-12-12', 0)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces) 
  VALUES(5, 1, 14, 'Opis', '2016-05-30', '2016-12-21', 1)
INTO REALIZACJAPROJ(IdProjektu, IdFazy, IdPracownika, OpisZadania, DataRozp, DataZak, CzySukces)
  VALUES(5, 1, 13, 'Opis', '2016-06-04', NULL, NULL)
SELECT * FROM DUAL;



--DELETE FROM MIASTO;
--COMMIT;





