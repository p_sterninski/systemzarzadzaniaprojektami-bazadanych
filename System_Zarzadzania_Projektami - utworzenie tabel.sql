-- Create new table STATUSFAZYPROJ.
-- STATUSFAZYPROJ : Table of StatusFazyProj
-- 	IDFAZY : IdFazy identyfikuje StatusFazyProj
-- 	IDPROJEKTU : IdProjektu cz�ciowo identyfikuje StatusFazyProj
-- 	DATAROZP : DataRozp z StatusFazyProj
-- 	DATAZAK : DataZak z StatusFazyProj
-- 	CZYSUKCES : CzySukces z StatusFazyProj  
create table STATUSFAZYPROJ (
	IDFAZY INT not null,
	IDPROJEKTU INT not null,
	DATAROZP DATE not null,
	DATAZAK DATE null,
	CZYSUKCES INT null, constraint STATUSFAZYPROJ_PK primary key (IDFAZY, IDPROJEKTU) ); 

-- Create new table FAZAPROJ.
-- FAZAPROJ : Table of FazaProj
-- 	IDFAZY : IdFazy identyfikuje FazaProj
-- 	NAZWA : Nazwa z FazaProj  
create table FAZAPROJ (
	IDFAZY INT not null,
	NAZWA VARCHAR2(20) not null, constraint FAZAPROJ_PK primary key (IDFAZY) ); 

-- Create new table UMIEJETNOSCPRAC.
-- UMIEJETNOSCPRAC : Table of UmiejetnoscPrac
-- 	IDPRACOWNIKA : IdPracownika cz�ciowo identyfikuje UmiejetnoscPrac
-- 	NAZWASKR : NazwaSkr cz�ciowo identyfikuje UmiejetnoscPrac  
create table UMIEJETNOSCPRAC (
	IDPRACOWNIKA INT not null,
	NAZWASKR VARCHAR2(10) not null, constraint UMIEJETNOSCPRAC_PK primary key (IDPRACOWNIKA, NAZWASKR) ); 

-- Create new table UMIEJETNOSC.
-- UMIEJETNOSC : Table of Umiejetnosc
-- 	NAZWASKR : NazwaSkr identyfikuje Umiejetnosc
-- 	NAZWAPELNA : NazwaPelna z Umiejetnosc  
create table UMIEJETNOSC (
	NAZWASKR VARCHAR(10) not null,
	NAZWAPELNA VARCHAR2(20) not null, constraint UMIEJETNOSC_PK primary key (NAZWASKR) ); 

-- Create new table MIASTO.
-- MIASTO : Table of Miasto
-- 	IDMIASTA : IdMiasta identyfikuje Miasto
-- 	NAZWA : Nazwa z Miasto  
create table MIASTO (
	IDMIASTA INT not null,
	NAZWA VARCHAR2(20) not null, constraint MIASTO_PK primary key (IDMIASTA) ); 

-- Create new table PRACOWNIK.
-- PRACOWNIK : Table of Pracownik
-- 	IDPRACOWNIKA : IdPracownika identyfikuje Pracownik
-- 	IMIE : Imie z Pracownik
-- 	NAZWISKO : Nazwisko z Pracownik
-- 	DATAURODZENIA : DataUrodzenia z Pracownik
-- 	ADRES : Adres z Pracownik
-- 	IDMIASTA : IdMiasta z Pracownik  
create table PRACOWNIK (
	IDPRACOWNIKA INT not null,
	IMIE VARCHAR2(20) not null,
	NAZWISKO VARCHAR2(20) not null,
	DATAURODZENIA DATE not null,
	ADRES VARCHAR2(30) not null,
	IDMIASTA INT not null, constraint PRACOWNIK_PK primary key (IDPRACOWNIKA) ); 

-- Create new table PRACZEWN.
-- PRACZEWN : Table of PracZewn
-- 	IDPRACOWNIKA : IdPracownika identifies PracZewn
-- 	EMAIL : Email z PracZewn
-- 	TELEFON : Telefon z PracZewn
-- 	INNYKONTAKT : InnyKontakt z PracZewn  
create table PRACZEWN (
	IDPRACOWNIKA INT not null,
	EMAIL VARCHAR2(20) null,
	TELEFON VARCHAR2(20) null,
	INNYKONTAKT VARCHAR2(20) null, constraint PRACZEWN_PK primary key (IDPRACOWNIKA) ); 

-- Create new table PRACWEWN.
-- PRACWEWN : Table of PracWewn
-- 	IDPRACOWNIKA : IdPracownika identifies PracWewn
-- 	STANOWISKO : Stanowisko z PracWewn  
create table PRACWEWN (
	IDPRACOWNIKA INT not null,
	STANOWISKO VARCHAR2(20) not null, constraint PRACWEWN_PK primary key (IDPRACOWNIKA) ); 

-- Create new table REALIZACJAPROJ.
-- REALIZACJAPROJ : Table of RealizacjaProj
-- 	IDPROJEKTU : IdProjektu cz�ciowo identyfikuje RealizacjaProj
-- 	IDFAZY : IdFazy cz�ciowo identyfikuje RealizacjaProj
-- 	IDPRACOWNIKA : IdPracownika cz�ciowo identyfikuje RealizacjaProj
-- 	OPISZADANIA : OpisZadania cz�ciowo identyfikuje RealizacjaProj
-- 	DATAROZP : DataRozp z RealizacjaProj
-- 	DATAZAK : DataZak z RealizacjaProj
-- 	CZYSUKCES : CzySukces z RealizacjaProj  
create table REALIZACJAPROJ (
	IDPROJEKTU INT not null,
	IDFAZY INT not null,
	IDPRACOWNIKA INT not null,
	OPISZADANIA VARCHAR2(50) not null,
	DATAROZP DATE not null,
	DATAZAK DATE null,
	CZYSUKCES INT null, constraint REALIZACJAPROJ_PK primary key (IDFAZY, IDPROJEKTU, IDPRACOWNIKA) ); 

-- Create new table PROJEKT.
-- PROJEKT : Table of Projekt
-- 	IDPROJEKTU : IdProjektu identyfikuje Projekt
-- 	NAZWA : Nazwa z Projekt
-- 	DATAROZP : DataRozp z Projekt
-- 	DATAZAK : DataZak z Projekt
-- 	CZYSUKCES : CzySukces z Projekt
-- 	KIEROWNIKPROJ : KierownikProj z Projekt
-- 	ZASTKIEROWNIKA : ZastKierownika z Projekt  
create table PROJEKT (
	IDPROJEKTU INT not null,
	NAZWA VARCHAR2(20) not null,
	DATAROZP DATE not null,
	DATAZAK DATE null,
	CZYSUKCES INT null,
	KIEROWNIKPROJ INT not null,
	ZASTKIEROWNIKA INT null, constraint PROJEKT_PK primary key (IDPROJEKTU) ); 

-- Add foreign key constraints to table STATUSFAZYPROJ.
alter table STATUSFAZYPROJ
	add constraint FAZAPROJ_STATUSFAZYPROJ_FK1 foreign key (
		IDFAZY)
	 references FAZAPROJ (
		IDFAZY); 

alter table STATUSFAZYPROJ
	add constraint PROJEKT_STATUSFAZYPROJ_FK1 foreign key (
		IDPROJEKTU)
	 references PROJEKT (
		IDPROJEKTU); 

-- Add foreign key constraints to table UMIEJETNOSCPRAC.
alter table UMIEJETNOSCPRAC
	add constraint PRACOWNIK_UMIEJETNOSCPRAC_FK1 foreign key (
		IDPRACOWNIKA)
	 references PRACOWNIK (
		IDPRACOWNIKA); 

alter table UMIEJETNOSCPRAC
	add constraint UMIEJ_UMIEJPRAC_FK1 foreign key (
		NAZWASKR)
	 references UMIEJETNOSC (
		NAZWASKR); 

-- Add foreign key constraints to table PRACZEWN.
alter table PRACZEWN
	add constraint PRACOWNIK_PRACZEWN_FK1 foreign key (
		IDPRACOWNIKA)
	 references PRACOWNIK (
		IDPRACOWNIKA); 

-- Add foreign key constraints to table PRACWEWN.
alter table PRACWEWN
	add constraint PRACOWNIK_PRACWEWN_FK1 foreign key (
		IDPRACOWNIKA)
	 references PRACOWNIK (
		IDPRACOWNIKA); 

-- Add foreign key constraints to table PRACOWNIK.
alter table PRACOWNIK
	add constraint MIASTO_PRACOWNIK_FK1 foreign key (
		IDMIASTA)
	 references MIASTO (
		IDMIASTA); 

-- Add foreign key constraints to table REALIZACJAPROJ.
alter table REALIZACJAPROJ
	add constraint STATFAZPROJ_REALIZPROJ_FK1 foreign key (
		IDFAZY,
		IDPROJEKTU)
	 references STATUSFAZYPROJ (
		IDFAZY,
		IDPROJEKTU); 

alter table REALIZACJAPROJ
	add constraint PRACOWNIK_REALIZACJAPROJ_FK1 foreign key (
		IDPRACOWNIKA)
	 references PRACOWNIK (
		IDPRACOWNIKA); 

-- Add foreign key constraints to table PROJEKT.
alter table PROJEKT
	add constraint PRACOWNIK_PROJEKT_FK1 foreign key (
		KIEROWNIKPROJ)
	 references PRACOWNIK (
		IDPRACOWNIKA); 

alter table PROJEKT
	add constraint PRACOWNIK_PROJEKT_FK2 foreign key (
		ZASTKIEROWNIKA)
	 references PRACOWNIK (
		IDPRACOWNIKA); 


-- This is the end of the Microsoft Visual Studio generated SQL DDL script.
